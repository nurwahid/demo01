FROM maven:3.8.1-jdk-8-slim 
RUN mkdir /opt/demo
WORKDIR /opt/demo
COPY . .
RUN unset MAVEN_CONFIG && env && ./mvnw package
RUN mv target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]
